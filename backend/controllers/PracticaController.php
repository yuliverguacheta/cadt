<?php

namespace backend\controllers;

use common\models\LoginForm;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use backend\models\Vacantes;
use yii\data\Pagination;
/**
 * Site controller
 */
class PracticaController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'menulist', 'newitem', 'mail'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','menulist', 'newitem', 'mail'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionMenulist()
    {
        $query = Vacantes::find();

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $nombre = $query->orderBy('nombre')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('/practica/menulist', [
            'nombre' => $nombre,
            'pagination' => $pagination,
        ]);
    }

    public function actionNewitem()
    {
        return $this->render('newitem');
    }

    public function actionMail()
    {   
        return $this->render('mail');

    }
       
}