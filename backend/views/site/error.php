<?php

/** @var yii\web\View $this */
/** @var string $name */
/** @var string $message */
/** @var Exception $exception*/

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        cada que ejecuto esta accion terminamos con los pelos de punta me ois?
        Gracias...
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you. <br>
        Por favor contactanos si piensas que existe algun error con el servidor, Gracias mi chinin
    </p>

</div>
