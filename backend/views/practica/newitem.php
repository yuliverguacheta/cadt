<?php

/** @var yii\web\View $this */
$message = Yii::$app->mailer->compose();
$this->title = 'Informacion de Practicas y Pasantias';
?>
<div class="col-xs-12">
    <div class="box box-info">
        
    <form class="content" method = 'post' action = "/practica/mail">
            <div class="container-fluid">
                <div class="col-xs-10">
                    <div class="row"></div>
                    <div class="row"></div>
                    <div class="table-responsive">
                        <small>
                            <table class="table table-striped">

                                <thead>
                                    <tr>
                                        <th colspan="12" style="text-align: center; background-color: #034C6F;
                                                                 color: #dae9ec; padding: 10px; font-size: 16px;">
                                            POSTULARSE 
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>



                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <td colspan="2" style="text-align: left; background-color: #0f6893; color: #b8c7ce">
                                                        LLene la informacion Requerida
                                                    </td>
                                                </tr>
                                            </thead>
                                            
                                            <div class="row">
                                                <div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                                                    <div class="block block-alt-noborder">
                                                        <article>
                                                            <div class="table-responsive">
                                                                
                                                                <table class="table table-borderless table-hover" id="tableA">
                                                                    <tbody>
                                                                        <!-- <tr style="opacity: 1; ">
                                                                            <td width='30'>
                                                                                <label> Nombre: </label>
                                                                            </td>
                                                                            <td style="vertical-align: middle;" colspan="6"> Edison </td>
                                                                        </tr> -->

                                                                        <tr class="opacity: 0.6; ">
                                                                            <td>
                                                                                <label> Id </label>
                                                                            </td>
                                                                            <td class="vertical-align: middle;" colspan="6">
                                                                                <div class="form-group has-feedback">
                                                                                    <input name="id_p" type="text" class="form-control" placeholder="Escriba su correo institucional"
                                                                                            disabled="desabled" value="12">
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                        <tr class="opacity: 0.6; ">
                                                                            <td>
                                                                                <label> Codigo Estudiante: </label>
                                                                            </td>
                                                                            <td class="vertical-align: middle;" colspan="6">
                                                                                <div class="form-group has-feedback">
                                                                                    <input name="cd_estudiante" type="text" class="form-control" placeholder="Escriba su Codigo de estudiante" 
                                                                                    value="">
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                        <tr style="opacity: 1; ">
                                                                            <td width='30'>
                                                                                <label> Nombre: </label>
                                                                            </td>
                                                                            <td id="td_c_personal" style="vertical-align: middle;" colspan="6">
                                                                                <div class="form-group has-feedback">
                                                                                    <input name="Nombre" name="c_personal" type="text" require class="form-control"
                                                                                            placeholder="Nombre Estudiante" value="">
                                                                                        <i class="glyphicon glyphicon-pencil form-control-feedback"></i>
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                        <tr style="opacity: 1; ">
                                                                            <td width='30'>
                                                                                <label> Facultad: </label>
                                                                            </td>
                                                                            <td id="faculad" style="vertical-align: middle;" colspan="6">
                                                                                <div class="form-group has-feedback">
                                                                                    <input name="c_personal" name="c_personal" type="text" require class="form-control"
                                                                                            placeholder="Facultad Estudiante" value="">
                                                                                        <i class="glyphicon glyphicon-pencil form-control-feedback"></i>
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                        <tr style="opacity: 1; ">
                                                                            <td width='30'>
                                                                                <label> Categoria: </label>
                                                                            </td>
                                                                            <td id="categria" style="vertical-align: middle;" colspan="6">
                                                                                <div class="form-group has-feedback">
                                                                                    <input name="c_personal" name="c_personal" type="text" require class="form-control"
                                                                                            placeholder="Pasantia o Tesis" value="">
                                                                                        <i class="glyphicon glyphicon-pencil form-control-feedback"></i>
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                            <div id="j_idt10:buttons" class="form-group form-actions text-center">
                                                                <ul class="list-inline">
                                                                    <li>
                                                                        <div class="form-group">
                                                                            <button type="submit" id="btn" class="btn btn-info next-step1">
                                                                            <!-- <span class="ui-button-icon-left ui-icon ui-c fa fa-check-square-o"></span> -->
                                                                                <span class="ui-button-text ui-c" 
                                                                                > Enviar </span>
                                                                            </button>   
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                        </article>
                                                    </div>
                                                </div>
                                            </div>

                                        </table>

                                </tbody>

                            </table>
                        </small>
                    </div>
                </div>
            </div>
    </form>

    </div>
</div>