<?php

/** @var yii\web\View $this */

$this->title = 'Informacion de Practicas y Pasantias';
$db = new yii\db\Connection;
use yii\db\Query;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use backend\models\Vacantes;
use backend\controllers\VacanteController;
?>
<div class="col-xs-12">
    <div class="box box-info">
        <section class="content">
            <div class="container-fluid">
                <div class="col-xs-10">
                    <div class="row"></div>
                    <div class="row"></div>

                    <div class="table-responsive">
                        <small>
                            <table class="table table-striped">

                                <thead>
                                    <tr>
                                        <th colspan="12" style="text-align: center; background-color: #034C6F;
                                                                 color: #dae9ec; padding: 10px; font-size: 16px;">
                                        Listado de Pasantias y Practicas Disponibles
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td colspan="6">
                                            <a name="Pasantias"></a>

                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <td colspan="2" style="text-align: left; background-color: #0f6893; color: #b8c7ce">
                                                        Pasantias Disponibles --Primer Semestre 2022
                                                        </td>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table class="table table-striped table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="5" style="text-align: center;">
                                                                            Disponibles
                                                                        </th>
                                                                        <td style="background-color: #ffff;" width="1">
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <th width = "2%" >No</th>
                                                                        <th width = "25%" >Facultad</th>
                                                                        <th width = "10%" >Codigo</th>
                                                                        <th width = "50%" >Nombre</th>
                                                                        <th width = "50%" >Postulate</th>

                                                                        <td style="background-color: #ffff;"></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>      
                                                                <?php foreach ($nombre as $nombre): ?>               
                                                                <?php if ($nombre->categoria == 'Pasantia') {?>               
                                                                    <tr class="T1">
                                                                        <td>
                                                                            <ul>
                                                                                <?= Html::encode("{$nombre->id}") ?>
                                                                                
                                                                            </ul>
                                                                        </td>
                                                                        <td  id="t01">
                                                                            <span class="glyphicon glyphicon-education" 
                                                                                    data-toggle="tooltip" data-placement="right" title 
                                                                                    style="font-size: 13px; color: #61b254;" 
                                                                                    "data-original-title='Aprobado'">
                                                                                        <?= Html::encode("{$nombre->facultad}") ?>
                                                                                    
                                                                            </span>
                                                                        </td>

                                                                        <td> <a name="Codigo..."></a>
                                                                            <ul>
                                                                                <?= Html::encode("{$nombre->codigo}") ?>
                                                                                
                                                                            </ul>
                                                                        </td>

                                                                        <td> 
                                                                            <?= Html::encode("{$nombre->nombre}") ?>                                                                            
                                                                        </td>

                                                                        <td> 
                                                                            <p>
                                                                                <?= Html::a('Postularme', ['newitem'], ['class' => 'btn btn-info next-step1']) ?>
                                                                            </p>
                                                                        </td>

                                                                        <td>    
                                                                        <?php }?>
                                                                        <?php endforeach; ?>   
                                                                        </td>

                                                                        <td style="background-color: #ffff;"></td>
                                                                    </tr>

                                                                </tbody>
                                                                
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>

                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <td colspan="2" style="text-align: left; background-color: #0f6893; color: #b8c7ce">
                                                        Trabajos de grado Disponibles --Primer Semestre 2022
                                                        </td>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table class="table table-striped table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="5" style="text-align: center;">
                                                                            Disponibles
                                                                        </th>
                                                                        <td style="background-color: #ffff;" width="1">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width = "2%" >No</th>
                                                                        <th width = "25%" >Facultad</th>
                                                                        <th width = "10%" >Codigo</th>
                                                                        <th width = "50%" >Nombre</th>
                                                                        <th width = "50%" >Postulate</th>

                                                                        <td style="background-color: #ffff;"></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php if ($nombre->categoria == 'Tesis') {?>               
                                                                    <tr class="T1">
                                                                        <td>
                                                                            <ul>
                                                                                <?= Html::encode("{$nombre->id}") ?>
                                                                                
                                                                            </ul>
                                                                        </td>
                                                                        <td id="t01">
                                                                            <span class="glyphicon glyphicon-education" 
                                                                                    data-toggle="tooltip" data-placement="right" title 
                                                                                    style="font-size: 13px; color: #61b254;" 
                                                                                    "data-original-title='Aprobado'">
                                                                                    <?= Html::encode("{$nombre->facultad}") ?> 
                                                                            </span>
                                                                        </td>
                                                                        <td> <a name="Codigo..."></a>
                                                                            <ul>
                                                                                <?= Html::encode("{$nombre->codigo}") ?>
                                                                                
                                                                            </ul>
                                                                        </td>
                                                                        <td> 
                                                                            <?= Html::encode("{$nombre->nombre}") ?>
                                                                            <?php }?>
                                                                        </td>
                                                                            
                                                                        <td> 
                                                                            <p>
                                                                                <?= Html::a('Postularme', ['newitem'], ['class' => 'btn btn-info next-step1']) ?>
                                                                            </p>
                                                                        </td>
                                                                        
                                                                        
                                                                        <td style="background-color: #ffff;"></td>
                                                                    </tr>
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>

                                        </td>   
                                    </tr>
                                </tbody>

                            </table>
                        </small>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>