<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%vacantes}}".
 *
 * @property int $id
 * @property string $codigo
 * @property string $nombre
 * @property string $facultad
 * @property string $categoria
 */
class Crud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vacantes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'nombre', 'facultad', 'categoria'], 'required'],
            [['codigo'], 'string', 'max' => 10],
            [['nombre', 'categoria'], 'string', 'max' => 50],
            [['facultad'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'facultad' => 'Facultad',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\VacantesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VacantesQuery(get_called_class());
    }
}
