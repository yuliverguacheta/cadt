<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [

        'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'viewPath' => '@common/mail',
			'useFileTransport' => false,

			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'smtp.gmail.com',
				'username' => 'Practica@practica.com',
				'password' => 'Practica',
				'port' => '587',
				'encryption' => 'ssl',
			],
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'request' => [
            'enableCsrfValidation'=>false,
        ],

    ],
];